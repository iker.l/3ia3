#!/usr/bin/env python3

# EJERCICIO 1
nombre = "Iker"
print("Hola",nombre,", que tal") # Forma fácil 
print("Hola"+nombre+", que tal")
print(f"Hola {nombre}, que tal")
print("Hola {}".format(nombre),", que tal")

# ---------------------------------------------------
# EJERCICIO 2
nombre = "iker"
print(nombre.lower())
print(nombre.upper())
print(nombre.capitalize())

# -------------------------------------------

print('Roy Batty dijo en Blade Runner \"I\'ve seen things you people wouldn\'t believe. Attack ships on fire off the shoulder of Orion. I watched C-beams glitter in the dark near the Tannhäuser Gate. All those moments will be lost in time, like tears in the rain. Time to die.\". FIN')

# ------------------------------------------

print()
print()
print()

persona_famosa = "Roy Batty"
mensaje = persona_famosa + ' dijo en Blade Runner "I\'ve seen things you people wouldn\'t believe. Attack ships on fire off the shoulder of Orion. I watched C-beams glitter in the dark near the Tannhäuser Gate. All those moments will be lost in time, like tears in the rain. Time to die.". FIN'
print(mensaje)

# ---------------------------------------------

cadena = " Cadena con espacios. Salto de línea\n \tSigue en otra línea tabulada " 
print(cadena)
print(cadena.lstrip())
print(cadena.rstrip())
print(cadena.strip()) #por ambos lados


