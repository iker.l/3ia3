#!/usr/bin/env python3

# Mi primera variable
mensaje = "Hola mundo!"

print(mensaje)

mensaje = "cruel"

print(mensaje)

# Python es de tipado dinamico, puede cambiar su tipo de valor
# en tiempo de ejecucion
myVar = 21
myVar = "string"

# Obtener el tipo de una variable
print(type(myVar))
# Salida:
#   <class 'str'>
# str significa que es un string o cadena de texto 

# Asignacion multiple
# Mismo valor para diferentes variables
x=y=z = 12
# En una misma línea valores y variables diferentes
a,b,c = 'iker',12,3.2
print(a) # Lo compruebo imprimiendo a

# Nombres de variables
# Los nombres de variables sólo pueden contener letras, números y el guion bajo. Pueden empezar por una letra o el guion bajo, pero nunca con un número.
# ERROR! nombre de variable no permitido
#1var1 = 12122
_var2 = 1223 #ok
var_1 = 123
# ERROR! NO puede contener espacios
#var iable = 'sdsssdad'

# https://www.w3schools.com/python/ref_func_isinstance.asp
var_num = 123
print(isinstance(var_num,int)) # TRUE
print(isinstance(3.14,float))

# https://docs.python.org/es/3/library/functions.html?highlight=type#type
var_str = '23233'
print(type(var_str))


