#!/usr/bin/env python3

# Puede contener diferentes tipos 
bicicletas = [1,2,"BH","Orbea",True]
print(bicicletas) # [1, 2, 'BH', 'Orbea', True]

bicicletas = ["BH","Orbea"]
print(bicicletas[0])

# upper()
print(bicicletas[0].lower())

# ERROR
#test = [1,2,3]
#print(test[0].upper())

bicicletas = ["BH","Orbea","BMX"]
print(bicicletas[-1]) # Ultimo elemento
print(bicicletas[:-1]) # ['BH', 'Orbea']
print(bicicletas[1:2])



